**Created a GitLab Pages website from scratch.**


The link for the tutorial
https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html


Resources

- Keyword reference for the .gitlab-ci.yml file - https://docs.gitlab.com/ee/ci/yaml/

- Predefined variables reference - https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
